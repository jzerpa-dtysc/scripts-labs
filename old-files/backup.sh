#!/bin/sh

###############################################################################
# This function generates 
#
# Parameters: 
#
# Return: 
###############################################################################
sincronize(){ 
	#sincroniza y descargar la carpeta files a nuestra carpeta local
	#drush -y rsync @$projectCode.dev:%files/ @$projectCode.local:
	drush -y rsync @$projectCode.prod:%files/ @$projectCode.local:
	#drush -y rsync @educatic.dev:%files/ @educatic.local:
}

###############################################################################
# This function generates 
#
# Parameters: 
#
# Return: 
###############################################################################
compress(){
#para comprimir el directorio que necesitamos para el backup con el formato de la fecha correcto
proyecto=$projectCode
backup=$proyecto$fecha
echo "comprimiendo $proyecto"
#cd /home/dportela/projects/files/
tar czf /home/dportela/projects/backup/backup_$backup.tar.gz $proyecto/

#cd /home/dportela/projects/files
}


###############################################################################
# This function generates for 
#
# Parameters: 
#
# Return: 
###############################################################################
deleteOld(){
#para saber que archivo es el mas antiguo dentro de los backups que tenemos para poder eliminarlo y tener solo un numero de copias manejables en el servidor labs
#primero averiguamos el numero de archivos que tenemos dentro del directorio de DROBO
cd /mnt/DroboFS/Shares/BackupSites/BackupProjects/$projectCode
numero=`ls |wc -l`
echo $numero
LIMITE=50
# si es mayor que 50 eliminamos el ultimo
 if [ "$numero" -gt "$LIMITE" ];
  then
	eliminar=`ls -Utlr | awk '{print $9}' | head -n 2 | tail -n 1`
	#rm $eliminar
 else
	echo "Backup tiene menos de 50 archivos"
 fi
}

###############################################################################
# This function generates a email
#
# Parameters: 
#
# Return: 
###############################################################################
email(){
  #asunto
 EMAILMESSAGE="Almacenamiento drobo al límite revisar"
  #destinatario
  EMAIL="dportela.wikisaber@gmail.com" 
  SUBJECT="drobo space"
  #llamar a mail para enviarlo
  echo "$EMAILMESSAGE"|mail -s "$SUBJECT" $EMAIL
}
###############################################################################
# This function generates 
#
# Parameters: 
#
# Return: 
###############################################################################

delete(){
	ruta="/home/dportela/projects/backup/backup_$projectCode$fecha.tar.gz"
	rm $ruta
}
###############################################################################
# This function generates 
#
# Parameters: 
#
# Return: 
###############################################################################
sincronizeDrobo(){ 
	#nos colocamos en el directorio donde se almacenan los backups
	Server=root@10.0.0.69
	ruta="/home/dportela/projects/backup/backup_$projectCode$fecha.tar.gz"
	
	#rsync $ruta $Server:/mnt/DroboFS/Shares/Comun/backup2

	rsync $ruta $Server:/mnt/DroboFS/Shares/BackupSites/BackupProjects/$projectCode
	
}
###############################################################################
# This function generates 
#
# Parameters: 
#
# Return: 
###############################################################################
#spaceDrobo(){
#función para comprobar el tamaño en drobo
#cd /mnt/DroboFS/Shares/BackupSites/BackupProjects/$projectCode
#numero=`df -h| awk '{print $4}'| head -n 2 | tail -n 1 | cut -d "G" -f 1`
#LIMITE=100
#si el numero de GB disponibles es menor o igual a 100, mandar un email recordatorio
 #if [ "$numero" -le "$LIMITE" ];
  #then
#	email()
 #fi
#comando para que el dia 20 de cada mes nos mande un email recordatorio para mirar el espacio en drobo
# fecha=`date| awk '{print $3}'| head -n 2 | tail -n 1`
#limite=22
#if [ "$fecha" -eq "$limite" ];
  #then
#	email()
 #fi 

#}


###############################################################################
# Main code 
###############################################################################
#acceder al directorio donde se encuentra las carpetas preparas para el backup en labs
cd projects/files
#bucle para hacer las copias de seguridad de cada proyecto
 for i in *; do 
	projectCode=$i
	guion="_"
	dt=`date +%F-%H-%M`
	fecha="$guion$dt"
	echo $projectCode
	sincronize $projectCode
	compress $projectCode $fecha
	#enviar los datos a drobo
	sincronizeDrobo $projectCode $fecha
	#sincronizeCorrect $projectCode $fecha
	#delete los tar.gz de labs
	#delete $projectCode $fecha
 done
 #spaceDrobo()



