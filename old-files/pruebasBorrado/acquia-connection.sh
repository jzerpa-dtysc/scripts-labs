#!/bin/bash
###############################################################################
# Name: Acquia Hosting Connection
#
# Description: This script connects to the server through an Acquia key file. 
#    If this file does not exist this script will help to you to generate it.
#    Once generated the key file you must send the public key to Acquia 
#    manager servers for the key is released.
# 
#    Once generated the key file you must send the key administrator for 
#    Acquia servers that the key is released.
# 
# Parameters: Project-Code. The code-project must be equal to the unix server
#     user. Eg.: wisdem or encuentro.
#
# Author: Jorge Carballo
###############################################################################

###############################################################################
# This function generates the path when the key file will be saved.
#
# Parameters: $ProjectCode
#
# Return: $pathKeyFile
###############################################################################
getPathKeyFile(){	
	
	# Local user
        localUser="$(whoami)"

        # ssh folder
        sshFolder="/home/$localUser/.ssh"

        # Proyect Code
        projectCode=$1

        # KeyFile
        KeyFileName="${projectCode}_hosting"

        # Path key file
        pathKeyFile="$sshFolder/$KeyFileName"

        # Creating ssh folder:
        if [ ! -d $sshFolder ];
        then
           echo "Creating Folder $sshFolder to save the keyfile"
           mkdir $sshFolder
        fi

}


###############################################################################
# This function generates the key file that will be used to connect to 
# the Acquia Servers
#
# Parameters: $pathKeyFile
#
# Return: Public Key to send Acquia Hosting Manager (on screen)
###############################################################################
genereAcquiaKeyFile(){

	pathKeyFile=$1

        echo "Enter your passphrase to access in Acquia Hosting, then press Enter."
	echo -n "passphrase: "
        stty -echo
        read passphrase
        stty echo

        echo ""         # force a carriage return to be output

        ssh-keygen -t dsa -N "$passphrase" -f $pathKeyFile
	
	echo " ***************** Send this to the Acquia Manager ******************** \n"
        cat $pathKeyFile.pub
	echo " \n ***************** End of the key generated *************************** "
	

        chmod 600 $pathKeyFile

}

###############################################################################
# This function connects to the server using the Acquia key file suitable for 
#the project indicated.
#
# Parameters: $pathKeyFile
#
# Return: Acquia Server Shell 
###############################################################################
connectTo(){
	
	pathKeyFile=$1
	
	# jcarballo - remote user
	unixUser=$projectCode

	# jcarballo - Acquia Hosting
	host="srv-66.devcloud.hosting.acquia.com"

	# jcarballo - If the filekey can be read:
	if [ -r $filekey ];
	then

           echo "Connecting to $projectCode on Acquia..."

	   ssh -i $pathKeyFile $unixUser@$host

	else
	   echo "File $pathKeyFile can not be read"
	fi

}


###############################################################################
# Main code 
###############################################################################
if [ $# -eq 1 ]; 
then

	# Project Code: project to which we connect
	projectCode=$1

	getPathKeyFile $projectCode

	if [ -e $pathKeyFile ]
	then
		

		connectTo $pathKeyFile
	else
		echo "The keyfile does not exist, now it will be created on $pathKeyFile"

		genereAcquiaKeyFile $pathKeyFile

		connectTo $projectCode
	fi
else
	echo "You must pass the project-code as parameter. Eg wisdem"
fi
