#!/bin/sh
sincronize(){ 
	#sincroniza y descargar la carpeta files a nuestra carpeta local
	drush -y rsync @educatic.dev:%files/ @$projectCode.local:

}

compress(){
#para comprimir el directorio que necesitamos para el backup con el formato de la fecha correcto
proyecto=$projectCode
backup=$proyecto$fecha
echo "comprimiendo $proyecto"
#cd /home/dportela/projects/files/
tar czf /home/dportela/projects/backup/backup_$backup.tar.gz $proyecto/

}


sincronizeDrobo(){ 
	#nos colocamos en el directorio donde se almacenan los backups
	Server=root@10.0.0.69
	ruta="/home/dportela/projects/backup/backup_$projectCode$fecha.tar.gz"
	
	#rsync $ruta $Server:/mnt/DroboFS/Shares/Comun/backup2

	rsync $ruta $Server:/mnt/DroboFS/Shares/BackupSites/BackupProjects/$projectCode
	
}

cd projects/files
 for i in *; do
   projectCode=$i;
   guion="_";
   dt=`date +%F-%H-%M`
   fecha="$guion$dt"
   echo $projectCode
   pro="educatic"
   if [ $projectCode = "educatic" ];  then 
   	sincronize $projectCode
	compress $projectCode $fecha
	sincronizeDrobo $projectCode $fecha
   else
      echo no es el directorio que buscaba
   fi
 done
